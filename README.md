# WebBytes

WebBytes is a simple File Manager to transfer files to another device

## Usage

Java 17 (or newer) is required to run WebBytes

you can run WebBytes from the terminal using  
```java -jar WebBytes-1.0.jar```

## Build
to build WebBytes from source you can use the package.sh file  
```sh package.sh```  
this will package the webapp into a .jar  
and a .update for updaing WebBytes easier (WIP)
